var openModalButton = getOpenModalButton();
var closeModalButton = getCloseModalButton();
var okButton = getOkButton();
var modal = getModal();
var backdrop = getBackdrop();
var addNameInput = getNameInput();
var addJobTitleInput = getJobTitleInput();
var addAgeInput = getAgeInput();
var addNicknameInput = getNicknameInput();
var addEmployeeInput = getEmployeeInput();
var dumpedData = [];
var dumpedDataBox = getDumpedDataBox();


openModalButton.addEventListener('click', function () {
    modal.style.display = 'block';
    backdrop.style.display = 'block';
});

closeModalButton.addEventListener('click', closeModal);

backdrop.addEventListener('click', closeModal);

okButton.addEventListener('click', function () {
    savePerson();
    closeModal();
});

getDataFromJson();

function getDataFromJson() {

    var data = [{ "name": "Giacomo Guilizzoni1", "job": "Founder & CEO", "age": "34", "nick": "Peldi", "employee": true },
    { "name": "Guido Jach Guilizzoni", "job": "", "age": "4", "nick": "The Guids", "employee": false },
    { "name": "Marco Botton", "job": "Tuttofare", "age": "31", "nick": "", "employee": true },
    { "name": "Mariah Maclachlan", "job": "Better Half", "age": "35", "nick": "Patata", "employee": true },
    { "name": "Julianne Liss", "job": "Concrete Engineering Technician", "age": "51", "nick": "Jul", "employee": true },
    { "name": "Venus Drey", "job": "Construction Ironworker Helper", "age": "28", "nick": "Venus", "employee": true },
    { "name": "Elin Kalman", "job": "Sensor Operator", "age": "43", "nick": "Elin", "employee": true },
    { "name": "Tyler Zeledon", "job": "Airport Manager", "age": "31", "nick": "Ty", "employee": true },
    { "name": "Nydia Mothershed", "job": "Regional Planner", "age": "49", "nick": "Mom", "employee": true },
    { "name": "Margrett Miguez", "job": "University Librarian", "age": "42", "nick": "Margie", "employee": true },
    { "name": "Del Henriquez", "job": "Cargo Agent", "age": "40", "nick": "Del :)", "employee": true },
    { "name": "Jannette Acres", "job": "Public Health Social Worker", "age": "50", "nick": "Janie", "employee": false },
    { "name": "Un Hance", "job": "Deboner", "age": "28", "nick": "UNHCR", "employee": true },
    { "name": "Erich Krogh", "job": "Low Altitude Air Defense Officer", "age": "25", "nick": "Erik", "employee": true },
    { "name": "Devora Wight", "job": "Commercial Credit Reviewer", "age": "38", "nick": "Debbie", "employee": true },
    { "name": "Alphonse Burbidge", "job": "Geographer", "age": "31", "nick": "Alphie", "employee": true },
    { "name": "Naomi Mcquaig", "job": "Traffic Line Painter", "age": "49", "nick": "Naomi", "employee": false },
    { "name": "Benton Cora", "job": "Interior Surface Insulation Worker", "age": "44", "nick": "Bent", "employee": true },
    { "name": "Mark Stringer", "job": "Aoc Director Combat Plans Officer", "age": "55", "nick": "Stingie", "employee": true },
    { "name": "Giacomo Guilizzoni", "job": "COO, WOW! Division", "age": ":)", "nick": "Val", "employee": true }];

    var tbody = getTbody();

    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement('tr');

        for (var j = 0; j < 6; j++) {

            var td = document.createElement('td');
            var cellText = "";
            if (j == 0) { cellText = document.createTextNode(data[i].name) }
            else if (j == 1) { cellText = document.createTextNode(data[i].job) }
            else if (j == 2) { cellText = document.createTextNode(data[i].age) }
            else if (j == 3) { cellText = document.createTextNode(data[i].nick) }
            else if (j == 4) {
                var checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                if (data[i].employee == true) {
                    checkbox.checked = true;
                } else { checkbox.checked = false; }
                cellText = checkbox;
            }
            else if (j == 5) {
                var deleteButton = document.createElement('button');
                var deleteText = document.createTextNode('Delete');
                deleteButton.className = "delete-button";
                deleteButton.appendChild(deleteText);
                cellText = deleteButton;
            };
            td.appendChild(cellText);
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }

    var index, table = document.querySelector('.table');
    for (var n = 1; n < table.rows.length; n++) {

        table.rows[n].cells[5].onclick = function () {
            var c = confirm("Do you want to delete this Person?");
            if (c === true) {
                index = this.parentElement.rowIndex;
                table.deleteRow(index);
            }
        };

        table.rows[n].cells[4].readOnly = true;

    }
}

function savePerson() {
    var inputArray = [
        addNameInput.value,
        addJobTitleInput.value,
        addAgeInput.value,
        addNicknameInput.value,
        addEmployeeInput
    ];

    var checkbox = "";

    var tbody = getTbody();

    for (var i = 0; i < 1; i++) {
        var tr = document.createElement('tr');

        for (var j = 0; j < 6; j++) {

            var td = document.createElement('td');
            var cellText = "";
            if (j == 0) {
                if (inputArray[0] == "") {
                    alert('Please give your name!')
                } else {
                    cellText = document.createTextNode(inputArray[0])
                };
            }
            else if (j == 1) { cellText = document.createTextNode(inputArray[1]) }
            else if (j == 2) { cellText = document.createTextNode(inputArray[2]) }
            else if (j == 3) { cellText = document.createTextNode(inputArray[3]) }
            else if (j == 4) {
                checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                if (inputArray[4].checked == true) {
                    checkbox.checked = true;
                    checkbox.value = true;
                } else {
                    checkbox.checked = false;
                    checkbox.value = false;
                }
                cellText = checkbox;
            }
            else if (j == 5) {
                var deleteButton = document.createElement('button');
                var deleteText = document.createTextNode('Delete');
                deleteButton.className = "delete-button";
                deleteButton.appendChild(deleteText);
                cellText = deleteButton;
            };
            td.appendChild(cellText);
            tr.appendChild(td);
        }
    }
    tbody.appendChild(tr);

    var index, table = document.querySelector('.table');
    for (var n = 1; n < table.rows.length; n++) {

        table.rows[n].cells[5].onclick = function () {
            var c = confirm("Do you want to delete this Person?");
            if (c === true) {
                index = this.parentElement.rowIndex;
                table.deleteRow(index);
            }
        };
    }

    var text = "";
    var p = document.createElement('p');

    for (k = dumpedData.length; k < dumpedData.length + 1; k++) {
        dumpedData[k] =
            {
                "name": inputArray[0],
                "job": inputArray[1],
                "age": inputArray[2],
                "nick": inputArray[3],
                "employee": inputArray[4]
            }
        text = document.createTextNode
            ("[" +
            "name : " + inputArray[0] + ", " +
            "job : " + inputArray[1] + ", " +
            "age : " + inputArray[2] + ", " +
            "nick : " + inputArray[3] + ", " +
            "employee : " + checkbox.value + "]"
            );
        p.appendChild(text);
        dumpedDataBox.appendChild(p);
        break;
    }
}

function getTbody() {
    return document.querySelector('.tbody');
};

function getCloseModalButton() {
    return document.querySelector('.btn-cancel');
}

function getOkButton() {
    return document.querySelector('.btn-confirm');
}

function getBackdrop() {
    return document.querySelector('.backdrop');
}

function getModal() {
    return document.querySelector('.modal');
}

function getOpenModalButton() {
    return document.querySelector('.modal-control');
}

function closeModal() {
    modal.style.display = 'none';
    backdrop.style.display = 'none';
    setInputVauleNull();
}

function setInputVauleNull() {
    addNameInput.value = "";
    addJobTitleInput.value = "";
    addAgeInput.value = "";
    addNicknameInput.value = "";
    uncheck();
}

function uncheck() {
    addEmployeeInput.checked = false;
};

function getNameInput() {
    return document.querySelector('#add-name-input')
}

function getJobTitleInput() {
    return document.querySelector('#add-jobtitle-input')
}

function getAgeInput() {
    return document.querySelector('#add-age-input')
}

function getNicknameInput() {
    return document.querySelector('#add-nickname-input')
}

function getEmployeeInput() {
    return document.querySelector('#add-employee-input')
}

function getDumpedDataBox() {
    return document.querySelector('.data')
}